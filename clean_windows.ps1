
function scheduled(){
    $action = New-ScheduledTaskAction -Execute ".\clean_windows.ps1"
    $trigger = New-ScheduledTaskTrigger -Weekly 
    Register-ScheduledTask -Action $action -Trigger $trigger -TaskName "clean_window"
}
function deleted_scheduled(){
    Unregister-ScheduledTask -TaskName "clean_window"
}

function system_clean(){
    Write-Host "Empty trash bin"
    $bin = (New-Object -ComObject Shell.Application).NameSpace(10); $bin.items() | ForEach-Object {; 
        Write-Host "^""Deleting $($_.Name) from Recycle Bin"^"";
        Remove-Item $_.Path -Recurse -Force; 
    }
    Write-Host "Clear Windows temp files"
    if (Test-path -path  "$env:localappdata\Temp"){
        Remove-Item -Path "$env:localappdata\Temp\*"  -Force -Recurse

    }
    if (Test-path -path  "$env:WINDIR\Temp"){
        Remove-Item -Path "$env:WINDIR\Temp" -Force -Recurse
    }
    if (Test-path -path  "$env:TEMP"){
        Remove-Item -Path "$env:TEMP" -Force -Recurse
    }
    
    $windows_logs=("setupapi.log", "Panther\*", "inf\setupapi.app.log", "setupapi.offline.log", "Logs\SIH\*", "Traces\WindowsUpdate\*",
    "setupact.log", "setuperr.log", "Temp\CBS\*", "debug\PASSWD.LOG")
    foreach($windows_log in $windows_logs){
        if (Test-path -path  "$env:SystemRoot\$windows_log"){
            Remove-Item -Path "$env:SystemRoot\$windows_log" -Force -Recurse
            if ($windows_log -eq "Logs\SIH\*"){
                Write-Host "Windows Update Events Logs"
            }
            elseif ($windows_log -eq "Traces\WindowsUpdate\*" ){
                Write-Host "Windows Update Logs"
            }
            elseif($windows_log -eq 'setupact.log' -or $windows_log -eq 'setuperr.log' ){
                Write-Host "Clear Windows Deployment Upgrade Process Logs"
            }
            elseif($windows_log -eq  'ServiceProfiles\LocalService\AppData\Local\Temp\*.*\'){
                Write-Host "Clear system temp folder when no one is logged in"
            }
            elseif($windows_log -eq  'debug\PASSWD.LOG'){
                Write-Host "Clear Passwo Remove-Item change events"
            }
            elseif($windows_log -eq 'Temp\CBS\*'){
                Write-Host "Clear Windows update and SFC scan logs"
            }
            else{
                Write-Host "Clear Windows Setup Logs"
            }
        }
    }
    Write-Host "Clean Windows Defender scan history"
    if (Test-path -path  "$env:ProgramData\Microsoft\Windows Defender\Scans\History\"){
        Remove-Item "$env:ProgramData\Microsoft\Windows Defender\Scans\History\" -Force -Recurse
    }
    Write-Host "Clear user web cache database"
    if (Test-path -path  "$env:localappdata\Microsoft\Windows\WebCache"){
        Remove-Item  -Path $env:localappdata\Microsoft\Windows\WebCache\*.* -Force -Recurse
    }
    Write-Host  "Clear MSPaint MRU"
    reg delete "HKCU\Software\Microsoft\Windows\CurrentVersion\Applets\Paint\Recent File List" /va /f
    reg delete "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Applets\Paint\Recent File List" /va /f
    Write-Host "Clear Wo Remove-Itempad MRU"
    reg delete "HKCU\SOFTWARE\Microsoft\Windows\CurrentVersion\Applets\Wo Remove-Itempad\Recent File List" /va /f
    Write-Host "Clear Windows Search Assistant history"
    reg delete "HKCU\Software\Microsoft\Search Assistant\ACMru" /va /f
    Write-Host "Clear list of recent programs opened"
    reg delete "HKCU\Software\Microsoft\Windows\CurrentVersion\Explorer\ComDlg32\LastVisitedPidlMRU" /va /f
    reg delete "HKCU\Software\Microsoft\Windows\CurrentVersion\Explorer\ComDlg32\LastVisitedPidlMRULegacy" /va /f
    Write-Host "Remove Default Apps Associations"
    dism.exe /online /Remove-DefaultAppAssociations
}

function chrome_clean(){
    Write-Host "Clear Google Chrome crash reports"
    Remove-Item -Path "$env:localappdata\Google\Chrome\User Data\Crashpad\reports\" -Force -Recurse
    Remove-Item -Path"$env:localappdata\Google\CrashReports\" -Force -Recurse
    Write-Host "Clear all Chrome user data"
    Remove-Item -Path "$env:localappdata\Google\Chrome\User Data" -Force -Recurse
}

function firefox_clean(){
    Write-Host "Clear all Firefox user profiles, settings, and data"
    Remove-Item -Path "$env:localappdata\Mozilla\Firefox\Profiles"  -Force -Recurse
    Remove-Item -Path "$env:APPDATA\Mozilla\Firefox\Profiles"  -Force -Recurse
}

function usage_of(){
    Write-Host "
Usage: ./clean_windows.ps1 [OPTION] 
script cleanup Windows
    -a  Add the script to  schedule (Script will be run once a month)
    -d  Remove the script from  schedule
    "
}

function main(){
    system_clean
    #chrome_clean
    #firefox_clean
}
if ( $args[0] -eq '-a' ){
    scheduled
}
elseif ( $args[0] -eq '-d') {
    deleted_scheduled
}
elseif  ($null -eq $args[0] ) {
    main
}
else {
    usage_of
}
