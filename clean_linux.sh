#!/usr/bin/env bash
set -o pipefail

declare -A color 
color=(["green"]="\e[32m" ["red"]="\e[31m" ["white"]="\e[37m" ["blue"]="\e[34m" ["yellow"]="\x1b[33m")

scheduled(){
    if [ -f '/etc/cron.monthly/clean_linux.sh' ]; then
        echo "Script added to cron earlier" 
    else
        sudo cp 'clean_linux.sh' '/etc/cron.monthly/clean_linux.sh' && echo "Added to cron"
    fi
}   

deleted_scheduled(){
    [ -f '/etc/cron.monthly/clean_linux.sh' ] &&  sudo rm '/etc/cron.monthly/clean_linux.sh' && echo "Deletion from cron"
}

system_clean(){
    echo "Run command"; sudo apt -y autoremove; sudo apt -y autoclean; 
    echo -e '\n--- Clear all system logs'; sudo rm -rfv /var/log/*
    [ -f "$HOME/.zsh_history" ] && ( echo '--- Clear zsh history'; rm -vf "$HOME/.zsh_history")
    [ -f "$HOME/.bash_history" ] && ( echo '--- Clear bash history'; rm -vf "$HOME/.bash_history")
    echo -e '\n--- Clear old version kernel'
    mapfile -t kernels < <(dpkg -l linux-{image,headers}-"[0-9]*" | awk '/ii/{print $2}' | grep -ve "$(uname -r | sed -r 's/-[a-z]+//')"| sed 's/Nazwa//g')
    sudo apt purge -y "${kernels[@]}"
    #for kernel in "${kernels[@]}"; do  sudo apt purge -y "$kernel"; done
}

chrome_clean(){
    echo -e '\n--- Google Chrome Cache Files'
    [ -d "$HOME/cache/google-chrome/Default/Cache/Cache_Data" ] && sudo rm -rfv "$HOME/cache/google-chrome/Default/Cache/Cache_Data/"*  &>/dev/null
    echo -e '\n--- Clear Google Chrome browsing history'
    [ -d "${HOME}/.config/google-chrome/Default/" ] &&  rm -r "${HOME}/.config/google-chrome/Default/"
    [ -d "${HOME}/.cache/google-chrome/default" ] && rm -r "${HOME}/.cache/google-chrome/default"
}

firefox_clean(){
    echo -e '\n--- Clear Firefox cache'; 
    [ -d "${HOME}/.cache/mozilla/firefox/" ] && rm -rf "${HOME}"/.cache/mozilla/firefox/*.default/*
    echo -e '\n--- Clear browsing history (cookies, history, forms):'
    [ -d "${HOME}/.cache/mozilla/firefox/" ] && rm "${HOME}"/.mozilla/firefox/*release/*.sqlite 
    [ -d "${HOME}/.cache/mozilla/firefox/" ] && rm "${HOME}"/.mozilla/firefox/*default/sessionstore.js
}

gradle_clean(){
    echo -e '\n--- Clear Gradle cache'
    [ -d "${HOME}/.gradle/caches" ] && rm -rfv ~/.gradle/caches/ &> /dev/null
}

docker_clean(){
    echo -e '\n--- Clear Docker'
    if type "docker" &> /dev/null; then
        echo -e "Run command: ${color["blue"]}docker system prune${color["white"]}"
        docker system prune -af
        echo "Remove all stopped containers"; docker container prune -f
        echo "Remove all dangling images"; docker image prune -f
        echo "Remove all unused networks"; docker network prune -f
        echo "${color["green"]} Docker cleanup completed."
    fi
}

npm_clean(){
    echo -e '\n--- Clear NPM cache'
    if type "npm" &> /dev/null; then
       npm cache verify; npm cache clean --force 
    fi
}

python_clean(){
    echo -e '\n--- Clear Pyenv-VirtualEnv cache'
    [ "$PYENV_VIRTUALENV_CACHE_PATH" ] && rm -rfv "$PYENV_VIRTUALENV_CACHE_PATH" &>/dev/null
}

automating_cleanup_of_old_files(){
    #This script deletes files older than a certain number of days in a specified directory.
    TARGET_DIR="/path/to/directory"
    local DAYS_OLD=30    
    find $TARGET_DIR -type f -mtime +$DAYS_OLD -exec rm -f {} \;
    echo "Cleanup completed. Files older than $DAYS_OLD days have been deleted."
}

usage_of(){
    echo """
Usage: ./clean_linux.sh [OPTION] 
script cleanup Linux
    -a  Add the script to cron schedule (Script will be run once a month)
    -d  Remove the script from cron schedule
    """
}

main(){
    #system_clean
    #chrome_clean
    #firefox_clean
    #gradle_clean
    docker_clean
    #npm_clean
    #python_clean
}

if [ "$1" = '-a' ]; then
    scheduled
elif [ "$1" = '-d' ]; then
    deleted_scheduled
elif [ "$1" = '' ]; then
    main
else
    echo "Invalid option: $1"
    usage_of
fi
