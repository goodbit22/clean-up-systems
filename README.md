# Scripts to clean up your Linux or Windows operating system

## 1)Description of the scripts

    clean_linux.sh - script for linux for clean Linux
    1.Clean grandle and npm cache
    2.Deleting images not used for a long time, docker network interfaces
    3.Remove history, cache gooogle chrome and firefox
    4.Remove unnecessary versions of the kernel, clean up system logs and remove unneeded packages

    clean_windows.ps1 - script for windows to clean Windows

## 2)Running scripts

* bash clean_linux.sh (Linux)
* ./clean_windows.ps1 (Windows)

## 3) Warnings

* <https://superuser.com/questions/106360/how-to-enable-execution-of-powershell-scripts> (You have to set in powershell policy because the default is to block powershell scripts from running on Windows)
